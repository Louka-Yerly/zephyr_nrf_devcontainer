# Installation
pre-commit:
```sh
pre-commit install
pre-commit install --hook-type commit-msg
```

- commit:
```sh
# install package
npm install --save-dev @commitlint/cz-commitlint commitizen inquirer@8
npm install --save-dev @commitlint/config-conventional @commitlint/cli

# add the following in the package.json
# "scripts": {
# 	"commit": "git-cz"
# },
# "config": {
# 	"commitizen": {
# 		"path": "@commitlint/cz-commitlint"
# 	}
# },

echo module.exports = {extends: ['@commitlint/config-conventional']}; > commitlint.config.js

# commit change (ignore node_modules)
alias gitc="npm run commit"
gitc
```